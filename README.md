# README #

Smart Playback Controller frame work is used for integrating customised player controls and add-on functionalities for all players.

Version 1.0.0

**Framework integration steps**
 
* Add PlayerSkinner folder to the project
* Add PlayerImage folder in Images.xcassets of the project
* Implement PlayerSkinnerDelegate in <playerviewcontroller>, where the customised player to be integrated
* Assign a skinner object 

```
#!objective-c

@property (nonatomic, strong)PlayerSkinner *skinner;

```
* Create skinner object, set delegate and add the view over the player view  
```
#!objective-c

_skinner = [PlayerSkinner createPlayerSkinner];
[_skinner prepareSkinnerUI];
_skinner.delegate = self;
[self.view addSubview:_skinner];

```

**Add-on features 
**

* Press & swipe right  on the player to forward the  video
* Press & swipe left on the player to rewind the video 
* Press & swipe up on the player to volume up the video 
* Press & swipe down on the player to volume down the video 
* Player Control Customisation
* SeekBar Customisation  
* Showing cuepoints in seek bar

**Player Control Customisation**

Play, forward, backward, chrome cast. seek-bar  etc player controls can be shown or hide using following customised function.
```
#!objective-c

-(void) hidePlayButton:(BOOL)value ;
-(void) hideSeekBar:(BOOL)value ;
-(void) hideChromecastButton:(BOOL)value ;
-(void) hideNextPreviousButton:(BOOL)value ;
```
Following delegate functions can be overriding for handling events of player controls 

```
#!objective-c
- (void) handlePlayPauseButtonPressed:(UIButton*)sender ;
- (void) handlePlayheadSliderValueChanged:(UISlider *)sender;
- (void) handlePlayheadSliderTouchBegin:(UISlider *)sender;
- (void) handlePlayheadSliderTouchEnd:(UISlider *)sender;
- (void) handleFullScreenButtonPressed:(UIButton *)sender;
- (void) handleDoneButtonPressed:(UIButton *)sender ;
- (void) handleReplayButtonPressed:(UIButton *)sender ;
- (void) handleNextButtonPressed:(UIButton*)sender;
```

**SeekBar Customisation **

SeekBar design can  be customised with customised color bands. 


```
#!objective-c

    _skinner = [PlayerSkinner createPlayerSkinner];
	[_skinner prepareSkinnerUI];
	
	_skinner.markWidth = 3.0;
	_skinner.selectedBarColor = [UIColor whiteColor];
	_skinner.unselectedBarColor = [UIColor blackColor];
	_skinner.markColor = [UIColor redColor];
	
	[_skinner prepareSliderUi];

    _skinner.delegate = self;

```
**Showing cuepoints in seek bar 
**

Seek bar can be marked with ad spots by passing array of cue points and total duration of the playing video 

```
#!objective-c

-(void) refershSliderCuePoints:(NSArray *) cuePoints totalDuration:(float) duration;

```
**How to contribute?
**

Coming soon !!!

**Contact
**

sarath.vs@attinadsoftware.com

vineeth.thayyil@attinadsoftware.com

jithin.s@attinadsoftware.com