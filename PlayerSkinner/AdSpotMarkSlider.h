//
//  AdSpotMarkSlider.h
//  AdSpotMarkSlider
//


#import <UIKit/UIKit.h>

@interface AdSpotMarkSlider : UISlider
@property (nonatomic) UIColor *markColor;
@property (nonatomic) CGFloat markWidth;
@property (nonatomic) NSArray *markPositions;
@property (nonatomic) UIColor *selectedBarColor;
@property (nonatomic) UIColor *unselectedBarColor;
@property (nonatomic) UIImage *handlerImage;
@property (nonatomic) UIColor *handlerColor;
@end
