//
//  PlayerSkinner.m
//  SonyLiv
//
//  Created by vineeth.thayyil on 05/09/16.
//  Copyright © 2016 Accedo. All rights reserved.
//

#import "PlayerSkinner.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIView+AutolayoutExtensions.h"
#import "AdSpotMarkSlider.h"

@interface PlayerSkinner () <UIGestureRecognizerDelegate>
@end

@interface PlayerSkinner ()

@property (nonatomic, weak) IBOutlet UIView *topControlsContainer;
@property (nonatomic, weak) IBOutlet UIView *controlsContainer;
@property (nonatomic, weak) IBOutlet UIButton *playPauseButton;
@property (nonatomic, weak) IBOutlet UILabel *playheadLabel;
@property (nonatomic, weak) IBOutlet  AdSpotMarkSlider *playheadSlider;
@property (nonatomic, weak) IBOutlet UILabel *durationLabel;
@property (nonatomic, weak) IBOutlet UIView *fullscreenButton;
@property (nonatomic, weak) IBOutlet MPVolumeView *mpVolumeSlider;
@property (nonatomic, weak) IBOutlet UIButton *replayButton;
@property (nonatomic, weak) IBOutlet UIButton *forwardButton;
@property (nonatomic, weak) IBOutlet UIButton *backwardButton;
@property (nonatomic, weak) IBOutlet UIButton *chromeCastButton;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;

@property (nonatomic, strong) UIView *spinner;
@property (nonatomic, strong) NSTimer *controlTimer;

@property (nonatomic, assign) PlayerStatus status;
@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGPoint interStartPoint;





@end

@implementation PlayerSkinner


#pragma mark - Methods -

-(id) initWithFrame:(CGRect)frame{
	self=  [super initWithFrame:frame];
	
	
	[self setFrame:frame];
	
	
	
	
	return self;
	
}


-(id) initWithCoder:(NSCoder *)aDecoder {
	
	self=  [super initWithCoder:aDecoder];
	
	if (self) {
  
		
	}
	
	return self;
	
}

-(void) prepareSkinnerUI {
	[self setPlaybackStatusTo:PlayerStatusLoading];
	[self.playheadSlider setThumbImage: [UIImage imageNamed:@"thumb"] forState:UIControlStateNormal];
	[self.playheadSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderMinimum"] forState:UIControlStateNormal];
	[self.playheadSlider setMaximumTrackImage:[UIImage imageNamed:@"sliderMaximum"] forState:UIControlStateNormal];
	self.mpVolumeSlider.showsRouteButton = NO;
	self.mpVolumeSlider.showsVolumeSlider = YES;
	[self.mpVolumeSlider setVolumeThumbImage:[UIImage imageNamed:@"thumb"] forState:UIControlStateNormal];
	[self.mpVolumeSlider setMaximumVolumeSliderImage:[UIImage imageNamed:@"volume_slider_maximum"] forState:UIControlStateNormal];
	[self.mpVolumeSlider setMinimumVolumeSliderImage:[UIImage imageNamed:@"volume_slider_minimum"] forState:UIControlStateNormal];
	[self addSpinner];
	self.chromeCastButton.hidden = YES;
	[self configureSingleTap];
	[self addSwipeGestures];
	//self.playheadSlider.markPositions = [self cuePointCalculation:12.4 cuePoints:nil];
}

-(void) prepareSliderUi {
	self.playheadSlider.markWidth = self.markWidth;
	self.playheadSlider.selectedBarColor = self.selectedBarColor;
	self.playheadSlider.unselectedBarColor = self.unselectedBarColor;
	self.playheadSlider.markColor = self.markColor;
}

-(void) setPlaybackStatusTo:(PlayerStatus)status {
	
	_status = status;
	
	if (status == PlayerStatusPlaying) {
		self.playPauseButton.selected = YES;
	}else {
		self.playPauseButton.selected = NO;
	}
}

-(BOOL) isPlayingVideo {
	return _status == PlayerStatusPlaying;
}

#pragma mark - Gesture -

-(void) addSwipeGestures {
	
	
	UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
	
	[self addGestureRecognizer:panGesture];
	
}

-(UISlider*) findVolumeSlider {
	
	for (UIView *view in [self.mpVolumeSlider subviews]) {
		if ([view isKindOfClass:[UISlider class]]) {
			return(UISlider*) view;
		}
	}
	
	return nil;
	
}
static float kMaxChangeInDuration = 0.1f;
static float kMaxChangeInVolume   = 1.0f;

-(float) getdeviceHeight {
	
	return MIN([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
	
}

-(float) getdeviceWidth {
	
	return MAX([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
	
}

-(void) volumeSliderCalculation:(float) deltaY{
	float deviceHeight          = [self getdeviceHeight];
	float offsetChangeHeightPer = deltaY/deviceHeight;
	
	UISlider *volumeSlider = [self findVolumeSlider];
	float value = volumeSlider.value - offsetChangeHeightPer*kMaxChangeInVolume ;
	
	if (value >= 1) {
		value = 1;
	}
	if (value <= 0) {
		value = 0;
	}
	
	[volumeSlider setValue:value];
}

-(float) sliderMovementCalculation:(float) deltaX{
	float deviceWidth          = [self getdeviceWidth];
	float offsetChangeWidthPer = deltaX/deviceWidth;
	
	float value = _playheadSlider.value + offsetChangeWidthPer*kMaxChangeInDuration ;
	
	if (value >= 1) {
		value = 1;
	}
	if (value <= 0) {
		value = 0;
	}
	
	return value;
	
}

-(IBAction)handlePanGesture:(UIPanGestureRecognizer *)sender
{
	
	CGPoint translate = [sender translationInView:self];
	
	if(sender.state == UIGestureRecognizerStateBegan){
		self.startPoint = translate;
		self.interStartPoint = translate;
		
		
		if ([self.delegate respondsToSelector:@selector(playerSeekStart)]) {
			[self.delegate playerSeekStart];
			
		}
		
		
	}else  if(sender.state == UIGestureRecognizerStateEnded){
		
		//     CGPoint velocity   =  [sender velocityInView:self];
		
		CGPoint endPoint = translate;
		float deltaX = endPoint.x - self.startPoint.x ;
		float deltaY = endPoint.y - self.startPoint.y ;
		
		if (_status == PlayerStatusLoading) {
			return;
		}
		if (fabsf(deltaX) < fabsf(deltaY) ) {
			
			[self volumeSliderCalculation:deltaY];
			
		}else{
			if ([self.delegate respondsToSelector:@selector(seekPlayerToValue:)]) {
				[self.delegate seekPlayerToValue:[self sliderMovementCalculation:deltaX]];
			}
		}
		
	} else {
		
		CGPoint endPoint = translate;
		
		if (_status == PlayerStatusLoading) {
			return;
		}
		
		float deltaX = endPoint.x - self.interStartPoint.x ;
		float deltaY = endPoint.y - self.interStartPoint.y ;
		
		
		self.interStartPoint = translate;
		
		if (fabsf(deltaX) < fabsf(deltaY) ) {
			[self volumeSliderCalculation:deltaY];
			
		}else{
			if ([self.delegate respondsToSelector:@selector(updatePlayerTimeLabel:)]) {
				[self.delegate updatePlayerTimeLabel:[self sliderMovementCalculation:deltaX]];
			}
		}
	}
}


-(void) configureSingleTap {
	// Used for hiding and showing the controls.
	UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
	tapRecognizer.numberOfTapsRequired = 1;
	tapRecognizer.numberOfTouchesRequired = 1;
	tapRecognizer.delegate = self;
	[self addGestureRecognizer:tapRecognizer];
	
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
	// This makes sure that we don't try and hide the controls if someone is pressing any of the buttons
	// or slider.
	if([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UISlider class]])
	{
		return NO;
	}
	
	return YES;
}

- (void)tapDetected:(UIGestureRecognizer *)gestureRecognizer
{
	[self showHideControls];
}

-(void )showHideControls {
	if([self isPlayingVideo])
	{
		if(self.controlsContainer.alpha == 0.f)
		{
			[self fadeControlsIn];
		}
		else if (self.controlsContainer.alpha == 1.0f)
		{
			[self fadeControlsOut];
		}
	}
}





#pragma mark - update progress -

-(void) displayDuration:(NSTimeInterval) duration {
	self.durationLabel.text = [PlayerSkinner formatTime:duration];
	
}
-(void) updateCurrentTime:(NSTimeInterval) currentTime {
	self.playheadLabel.text = [PlayerSkinner formatTime:currentTime];
}

-(void) updateSliderValueTo:(float) percent {
	self.playheadSlider.value = isnan(percent) ? 0.0f : percent;
}

#pragma mark - Configure Buttons -

-(void) hidePlayButton:(BOOL)value {
	[self.playPauseButton setHidden:value];
}

-(void) hideSeekBar:(BOOL)value {
	[self.playheadLabel setHidden:value];
	[self.durationLabel setHidden:value];
	[self.playheadSlider setHidden:value];
}

-(void) hideChromecastButton:(BOOL)value {
	[self.chromeCastButton setHidden:value];
}

-(void) setChromeCastButtonSelected:(BOOL)value {
	[self.chromeCastButton setSelected:value];
}

-(void) hideNextPreviousButton:(BOOL)value {
	[self.forwardButton setHidden:value];
	[self.backwardButton setHidden:value];
}

#pragma mark - IBActions -

- (IBAction)handlePlayPauseButtonPressed:(UIButton *)sender
{
	if ([self.delegate respondsToSelector:@selector(handlePlayPauseButtonPressed:)]) {
		[self.delegate handlePlayPauseButtonPressed:sender];
	}
}

- (IBAction)handlePlayheadSliderValueChanged:(UISlider *)sender
{
	if ([self.delegate respondsToSelector:@selector(handlePlayheadSliderValueChanged:)]) {
		[self.delegate handlePlayheadSliderValueChanged:sender];
	}
}

- (IBAction)handlePlayheadSliderTouchBegin:(UISlider *)sender
{
	if ([self.delegate respondsToSelector:@selector(handlePlayheadSliderTouchBegin:)]) {
		[self.delegate handlePlayheadSliderTouchBegin:sender];
	}
	
}

- (IBAction)handlePlayheadSliderTouchEnd:(UISlider *)sender
{
	if ([self.delegate respondsToSelector:@selector(handlePlayheadSliderTouchEnd:)]) {
		[self.delegate handlePlayheadSliderTouchEnd:sender];
	}
}

- (IBAction)handleFullScreenButtonPressed:(UIButton *)sender
{
	if ([self.delegate respondsToSelector:@selector(handleDoneButtonPressed:)]) {
		[self.delegate handleDoneButtonPressed:sender];
	}
}

- (IBAction)handleDoneButtonPressed:(UIButton *)sender {
	
	
	if ([self.delegate respondsToSelector:@selector(handleDoneButtonPressed:)]) {
		[self.delegate handleDoneButtonPressed:sender];
	}
	
}

-(IBAction)handleReplayButtonPressed:(UIButton *)sender {
	
	if ([self.delegate respondsToSelector:@selector(handleReplayButtonPressed:)]) {
		[self.delegate handleReplayButtonPressed:sender];
	}
}

-(IBAction)handleNextButtonPressed:(id)sender {
	
	if ([self.delegate respondsToSelector:@selector(handleNextButtonPressed:)]) {
		[self.delegate handleNextButtonPressed:sender];
	}
}

-(IBAction)handlePreviousButtonPressed:(id)sender {
	
	if ([self.delegate respondsToSelector:@selector(handlePreviousButtonPressed:)]) {
		[self.delegate handlePreviousButtonPressed:sender];
	}
	
}

-(IBAction)handleChromeCastButtonSelected:(id)sender {
	if ([self.delegate respondsToSelector:@selector(handleChromeCastButtonSelected:)]) {
		[self.delegate handleChromeCastButtonSelected:sender];
	}
}



#pragma mark - Hide/Show Controls -

// ** Customize these values **
static NSTimeInterval const kViewControllerControlsVisibleDuration          = 5;
static NSTimeInterval const kViewControllerFadeControlsInAnimationDuration  = .1;
static NSTimeInterval const kViewControllerFadeControlsOutAnimationDuration = .2;

- (void)fadeControlsIn
{
	[UIView animateWithDuration:kViewControllerFadeControlsInAnimationDuration
					 animations:^{
						 
						 [self showControls];
						 
					 } completion:^(BOOL finished) {
						 
						 if(finished)
						 {
							 [self reestablishTimer];
						 }
						 
					 }];
}

- (void)fadeControlsOut
{
	[UIView animateWithDuration:kViewControllerFadeControlsOutAnimationDuration animations:^{
		
		[self hideControls];
		
	}];
}

- (void)hideControls
{
	self.controlsContainer.alpha = 0.0f;
	self.topControlsContainer.alpha = 0.0f;
}

- (void)showControls
{
	self.controlsContainer.alpha = 1.0f;
	self.topControlsContainer.alpha = 1.0f;
}

- (void)reestablishTimer
{
	[self.controlTimer invalidate];
	self.controlTimer = [NSTimer scheduledTimerWithTimeInterval:kViewControllerControlsVisibleDuration target:self selector:@selector(fadeControlsOut) userInfo:nil repeats:NO];
}


- (void)invalidateTimerAndShowControls
{
	[self.controlTimer invalidate];
	[self showControls];
}

#pragma mark Class Methods


+(id) createPlayerSkinner {
	
	PlayerSkinner *skinner = [[[NSBundle mainBundle] loadNibNamed:@"PlayerSkinner" owner:self options:nil] objectAtIndex:0] ;
	//[skinner initializeUI];
	
	return  skinner;
}

-(void) refershSliderCuePoints:(NSArray *) cuePoints totalDuration:(float) duration{
	self.playheadSlider.markPositions = [self cuePointCalculation:duration cuePoints:cuePoints];
	[self.playheadSlider setNeedsDisplay];
	
}

-(void) reDrawSlider{
	[self.playheadSlider setNeedsDisplay];
	
}

+ (NSString *)formatTime:(NSTimeInterval)timeInterval
{
	static NSNumberFormatter *numberFormatter;
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		
		numberFormatter = [[NSNumberFormatter alloc] init];
		numberFormatter.paddingCharacter = @"0";
		numberFormatter.minimumIntegerDigits = 2;
		
	});
	
	if (isnan(timeInterval) || !isfinite(timeInterval) || timeInterval == 0)
	{
		return @"00:00:00";
	}
	
	NSUInteger hours = floor(timeInterval / 60.0f / 60.0f);
	NSUInteger minutes = (NSUInteger)(timeInterval / 60.0f) % 60;
	NSUInteger seconds = (NSUInteger)timeInterval % 60;
	
	NSString *formattedMinutes = [numberFormatter stringFromNumber:@(minutes)];
	NSString *formattedSeconds = [numberFormatter stringFromNumber:@(seconds)];
	
	NSString *ret = nil;
	if (hours > 0)
	{
		ret = [NSString stringWithFormat:@"%@:%@:%@", @(hours), formattedMinutes, formattedSeconds];
	}
	else
	{
		ret = [NSString stringWithFormat:@"00:%@:%@", formattedMinutes, formattedSeconds];
	}
	
	return ret;
}

#pragma mark - Spinner section

- (void) addSpinner
{
	if( !_spinner )
	{
		
		_spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		[(UIActivityIndicatorView*)_spinner startAnimating];
		_spinner.translatesAutoresizingMaskIntoConstraints = NO;
		[ self addSubview: _spinner ];
		//[ self addConstraints: [ _spinner centerInView: self ] ];
		
		
		NSMutableArray *constraints = [ NSMutableArray new ];
		
		[ constraints addObject: [ NSLayoutConstraint constraintWithItem: self.spinner attribute: NSLayoutAttributeCenterX relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeCenterX multiplier: 1.0 constant: 0 ] ];
		[ constraints addObject: [ NSLayoutConstraint constraintWithItem: self.spinner attribute: NSLayoutAttributeCenterY relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeCenterY multiplier: 1.0 constant: 0 ] ];
		
		[ self addConstraints: constraints ];
		
		
	}
}

- (void) removeSpinner
{
	[ _spinner removeFromSuperview ];
	_spinner = nil;
}


-(NSArray *) cuePointCalculation:(float) totalDuration cuePoints:(NSArray *)cuePoints {
	NSMutableArray* cuePointPercentagePositionArray  = [NSMutableArray arrayWithCapacity:cuePoints.count];
	
	for (int i = 0; i<cuePoints.count; i++) {
		float eachCuepoint = [[cuePoints objectAtIndex:i] floatValue];
		float eachCuepercentagePosition = (eachCuepoint *100)/totalDuration;
		[cuePointPercentagePositionArray addObject:[NSNumber numberWithFloat:eachCuepercentagePosition]];
	}
	return  [cuePointPercentagePositionArray copy];
	
}

@end
