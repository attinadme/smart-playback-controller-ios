//
//  PlayerSkinner.h
//  SonyLiv
//
//  Created by vineeth.thayyil on 05/09/16.
//  Copyright © 2016 Accedo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PlayerStatus) {
	PlayerStatusLoading,
	PlayerStatusPlaying,
	PlayerStatusPaused
};

@protocol PlayerSkinnerDelegate <NSObject>

@optional

- (void) handlePlayPauseButtonPressed:(UIButton*)sender ;
- (void) handlePlayheadSliderValueChanged:(UISlider *)sender;
- (void) handlePlayheadSliderTouchBegin:(UISlider *)sender;
- (void) handlePlayheadSliderTouchEnd:(UISlider *)sender;
- (void) handleFullScreenButtonPressed:(UIButton *)sender;
- (void) handleDoneButtonPressed:(UIButton *)sender ;
- (void) handleReplayButtonPressed:(UIButton *)sender ;
- (void) handleNextButtonPressed:(UIButton*)sender;
- (void) handlePreviousButtonPressed:(UIButton*)sender;
- (void) handleChromeCastButtonSelected:(UIButton*)sender;
- (void) seekPlayerToValue:(float)value;
- (void) updatePlayerTimeLabel:(float)value;
- (void) playerSeekStart;

@end

@interface PlayerSkinner : UIView

@property (nonatomic,weak) id <PlayerSkinnerDelegate> delegate;

+(id) createPlayerSkinner;
-(void) prepareSkinnerUI ;
-(void) prepareSliderUi ;

-(void) displayDuration:(NSTimeInterval) duration;
-(void) updateCurrentTime:(NSTimeInterval) currentTime ;
-(void) updateSliderValueTo:(float) percent;

-(void) setPlaybackStatusTo:(PlayerStatus)status;

-(void) hidePlayButton:(BOOL)value ;
-(void) hideSeekBar:(BOOL)value ;
-(void) hideChromecastButton:(BOOL)value ;
-(void) setChromeCastButtonSelected:(BOOL)value ;
-(void) hideNextPreviousButton:(BOOL)value ;

- (void) addSpinner;
- (void) removeSpinner;
-(void) refershSliderCuePoints:(NSArray *) cuePoints totalDuration:(float) duration;
-(void) reDrawSlider;

@property (nonatomic) UIColor *markColor;
@property (nonatomic) CGFloat markWidth;
//@property (nonatomic) NSArray *markPositions;
@property (nonatomic) UIColor *selectedBarColor;
@property (nonatomic) UIColor *unselectedBarColor;

@end
